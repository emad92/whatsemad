from django.conf.urls import patterns, include, url
from django.contrib import admin

#from whatsemad.views import home


from django.shortcuts import render
from django.http import HttpResponseRedirect

from django import forms

from models import *

from yowsup.layers.interface                           import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_messages.protocolentities  import TextMessageProtocolEntity
from yowsup.layers.protocol_media.protocolentities  import ImageDownloadableMediaMessageProtocolEntity
from yowsup.layers.protocol_receipts.protocolentities  import OutgoingReceiptProtocolEntity
from yowsup.layers.protocol_media.protocolentities  import LocationMediaMessageProtocolEntity
from yowsup.layers.protocol_acks.protocolentities      import OutgoingAckProtocolEntity
from yowsup.layers.protocol_media.protocolentities  import VCardMediaMessageProtocolEntity


class EchoLayer(YowInterfaceLayer):

    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):

        if not messageProtocolEntity.isGroupMessage():
            if messageProtocolEntity.getType() == 'text':
                self.onTextMessage(messageProtocolEntity)
            elif messageProtocolEntity.getType() == 'media':
                self.onMediaMessage(messageProtocolEntity)
    
    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        ack = OutgoingAckProtocolEntity(entity.getId(), "receipt", "delivery")
        self.toLower(ack)

    def onTextMessage(self,messageProtocolEntity):
        receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom())
            
        outgoingMessageProtocolEntity = TextMessageProtocolEntity(
            messageProtocolEntity.getBody(),
            to = messageProtocolEntity.getFrom())

        in_message = InMessage()
        in_message.sender = messageProtocolEntity.getFrom()
        in_message.text = messageProtocolEntity.getBody()
        in_message.save()

        print("Echoing %s to %s" % (messageProtocolEntity.getBody(), messageProtocolEntity.getFrom(False)))

        #send receipt otherwise we keep receiving the same message over and over
        self.toLower(receipt)
        #self.toLower(outgoingMessageProtocolEntity)

    def onMediaMessage(self, messageProtocolEntity):
        if messageProtocolEntity.getMediaType() == "image":
            
            receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom())

            outImage = ImageDownloadableMediaMessageProtocolEntity(
                messageProtocolEntity.getMimeType(), messageProtocolEntity.fileHash, messageProtocolEntity.url, messageProtocolEntity.ip,
                messageProtocolEntity.size, messageProtocolEntity.fileName, messageProtocolEntity.encoding, messageProtocolEntity.width, messageProtocolEntity.height,
                messageProtocolEntity.getCaption(),
                to = messageProtocolEntity.getFrom(), preview = messageProtocolEntity.getPreview())

            print("Echoing image %s to %s" % (messageProtocolEntity.url, messageProtocolEntity.getFrom(False)))

            #send receipt otherwise we keep receiving the same message over and over
            self.toLower(receipt)
            self.toLower(outImage)

        elif messageProtocolEntity.getMediaType() == "location":

            receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom())

            outLocation = LocationMediaMessageProtocolEntity(messageProtocolEntity.getLatitude(),
                messageProtocolEntity.getLongitude(), messageProtocolEntity.getLocationName(),
                messageProtocolEntity.getLocationURL(), messageProtocolEntity.encoding,
                to = messageProtocolEntity.getFrom(), preview=messageProtocolEntity.getPreview())

            print("Echoing location (%s, %s) to %s" % (messageProtocolEntity.getLatitude(), messageProtocolEntity.getLongitude(), messageProtocolEntity.getFrom(False)))

            #send receipt otherwise we keep receiving the same message over and over
            self.toLower(outLocation)
            self.toLower(receipt)
        elif messageProtocolEntity.getMediaType() == "vcard":
            receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom())
            outVcard = VCardMediaMessageProtocolEntity(messageProtocolEntity.getName(),messageProtocolEntity.getCardData(),to = messageProtocolEntity.getFrom())
            print("Echoing vcard (%s, %s) to %s" % (messageProtocolEntity.getName(), messageProtocolEntity.getCardData(), messageProtocolEntity.getFrom(False)))
            #send receipt otherwise we keep receiving the same message over and over
            self.toLower(outVcard)
            self.toLower(receipt)




from yowsup.stacks import YowStack
from yowsup.layers import YowLayerEvent
from yowsup.layers.auth                        import YowCryptLayer, YowAuthenticationProtocolLayer, AuthError
from yowsup.layers.coder                       import YowCoderLayer
from yowsup.layers.network                     import YowNetworkLayer
from yowsup.layers.protocol_messages           import YowMessagesProtocolLayer
from yowsup.layers.protocol_media              import YowMediaProtocolLayer
from yowsup.layers.stanzaregulator             import YowStanzaRegulator
from yowsup.layers.protocol_receipts           import YowReceiptProtocolLayer
from yowsup.layers.protocol_acks               import YowAckProtocolLayer
from yowsup.layers.logger                      import YowLoggerLayer
from yowsup.common import YowConstants

class YowsupEchoStack(object):
    def __init__(self, credentials):
        layers = (
            EchoLayer,
            (YowAuthenticationProtocolLayer, YowMessagesProtocolLayer, YowReceiptProtocolLayer, YowAckProtocolLayer, YowMediaProtocolLayer),
            YowLoggerLayer,
            YowCoderLayer,
            YowCryptLayer,
            YowStanzaRegulator,
            YowNetworkLayer
        )

        self.stack = YowStack(layers)
        self.stack.setProp(YowAuthenticationProtocolLayer.PROP_CREDENTIALS, credentials)
        self.stack.setProp(YowNetworkLayer.PROP_ENDPOINT, YowConstants.ENDPOINTS[0])
        self.stack.setProp(YowCoderLayer.PROP_DOMAIN, YowConstants.DOMAIN)
        self.stack.setProp(YowCoderLayer.PROP_RESOURCE, YowConstants.RESOURCE)

    def start(self, c=3):
        self.stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
        try:
            self.stack.loop(count=c)
        except AuthError as e:
            print("Authentication Error: %s" % e.message)



class HomeForm(forms.Form):
    number = forms.CharField(label='Number', max_length=100)
    text = forms.CharField(label='Text')



CREDENTIALS = ("12066009209", "/YP98J4ia7gbY9MIHNgtvAu4XXs=")

def home(request):

    stack = YowsupEchoStack(CREDENTIALS)
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = HomeForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            #return HttpResponseRedirect('/thanks/')
            layer = stack.stack.getLayer(-1)


            out_message = OutMessage()
            out_message.reciever = form.cleaned_data['number']+"@s.whatsapp.net"
            out_message.text = form.cleaned_data['text']
            out_message.save()

            outgoingMessageProtocolEntity = TextMessageProtocolEntity(
            form.cleaned_data['text'],
            to = form.cleaned_data['number']+"@s.whatsapp.net")

            #layer.toLower(outgoingMessageProtocolEntity)

            stack.start()

            layer.toLower(outgoingMessageProtocolEntity)

            out_message.sent = True
            out_message.save()

    # if a GET (or any other method) we'll create a blank form
    else:
        stack.start()

        form = HomeForm()

    return render(request, 'home.html', {'form': form})

class ChatForm(forms.Form):
    text = forms.CharField(label='Text')



def chat(request, num):
    stack = YowsupEchoStack(CREDENTIALS)
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ChatForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            stack = YowsupEchoStack(CREDENTIALS)
            layer = stack.stack.getLayer(-1)


            out_message = OutMessage()
            out_message.reciever = num+"@s.whatsapp.net"
            out_message.text = form.cleaned_data['text']
            out_message.save()

            outgoingMessageProtocolEntity = TextMessageProtocolEntity(
            form.cleaned_data['text'],
            to = num+"@s.whatsapp.net")

            #layer.toLower(outgoingMessageProtocolEntity)

            stack.start(5)

            layer.toLower(outgoingMessageProtocolEntity)

            out_message.sent = True
            out_message.save()

    else :
        stack.start()

    # if a GET (or any other method) we'll create a blank form
    form = ChatForm()

    in_messages = InMessage.objects.filter(sender__contains=num)
    out_message = OutMessage.objects.filter(reciever__contains=num)

    messages = list(in_messages) + list(out_message)

    messages = sorted(messages, key = lambda m : m.created)[-10:]

    return render(request, 'chat.html', {'form': form, 'messages' : messages})



from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^chat/(?P<num>\d+)/$', chat, name='chat'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
