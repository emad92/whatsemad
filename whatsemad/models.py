from django.db import models
from django.contrib.auth.models import AbstractUser
from model_utils.models import TimeStampedModel
try :
    import utils
except:
    pass



class Message(TimeStampedModel):
    text = models.TextField(max_length=200)

    def __unicode__(self):
        return self.text


class InMessage(Message):
    sender = models.CharField(max_length=200)

    def __unicode__(self):
        return self.sender + " " + self.text


class OutMessage(Message):
    reciever = models.CharField(max_length=200)
    sent = models.BooleanField(default=False)

    def __unicode__(self):
        return self.reciever + " " + self.text
